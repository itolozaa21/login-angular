import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {HomeComponent} from '../app/components/home/home.component'
import {AuthGuard} from '../app/guards/auth.guard'
const routes: Routes = [
  { path: 'home', component: HomeComponent ,canActivate:[AuthGuard]},
  { path: '', component: HomeComponent ,canActivate:[AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
