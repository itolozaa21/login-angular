import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from './services/login.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  user:string;
  sesion$: Observable<any>;

  form: FormGroup;
  showPassword:boolean = false;
  constructor(private loginService: LoginService,
              private router: Router) {
    this.buildForm();
    this.actualizar();
  }
  

  ngOnInit(): void {
   
  }

  actualizar(){
    this.user = localStorage.getItem('currentUser');
  }

  logout(){
    localStorage.removeItem('currentUser');
    localStorage.removeItem('sesion');
    this.router.navigate(['/']);
    console.log(localStorage.getItem('sesion'))
  }

  private buildForm() {
    this.form = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
      type:new FormControl('E', [Validators.required]),
      shop:new FormControl(1, [Validators.required]),
    });
  }

  login(){
    this.loginService.login(this.form.value).subscribe((rta: any) => {
      if(rta.cod == 'S01'){
        const sesion = {
          token:rta.dat.token,
        }

        localStorage.setItem('sesion',JSON.stringify(sesion) );
        localStorage.setItem("currentUser",rta.dat.user.nombre);

        Swal.fire(rta.tit,'Bienvenido '+rta.dat.user.nombre,'success');
        this.router.navigate(['/home']);
        this.actualizar();
      }
    }, (errorServicio) => {
       Swal.fire(errorServicio.error.tit,errorServicio.error.msg,'warning');
    });
  }

}
