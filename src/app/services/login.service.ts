import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { IfStmt } from '@angular/compiler';
import { isNullOrUndefined } from 'util';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  login(usuario: any) {
    return this.http.post(`${this.baseUrl}/login`, usuario);
  }

  getCurrentUser(){
    let currentUser = localStorage.getItem('currentUser');
    if(currentUser != null || undefined ){
      return currentUser;
    }else{
      return null;
    }
  }
}
